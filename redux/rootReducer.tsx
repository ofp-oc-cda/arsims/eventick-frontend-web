import { combineReducers } from "@reduxjs/toolkit";

import auth from "@/redux/reducers/auth";

const rootReducer = combineReducers({
  auth,
});

export default rootReducer;

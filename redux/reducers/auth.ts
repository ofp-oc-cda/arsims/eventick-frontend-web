import { createSlice } from "@reduxjs/toolkit";

interface initialStateState {
    username: string,
    email: string,
    password: string,
    errors: any,
    loading: boolean
};

const initialState: initialStateState = {
    username: "",
    email: "",
    password: "",
    errors: null,
    loading: false
};

const Auth = createSlice({
    name: "auth",
    initialState: initialState,
    reducers: {
        setUsername(state, action) {
            state.username = action.payload;
        },
        setEmail(state, action) {
            state.email = action.payload;
        },
        setPassword(state, action) {
            state.password = action.payload
        },
        setErrors(state, action) {
            state.errors = action.payload;
        },
        setLoading(state, action) {
            state.loading = action.payload;
        }
    }
});

export default Auth.reducer;

export const {
    setUsername,
    setEmail,
    setPassword,
    setErrors,
    setLoading
} = Auth.actions
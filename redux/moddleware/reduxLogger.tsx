import {Middleware} from "redux"
import {RootState} from "@/redux/store";

export const reduxLogger: Middleware<{}, RootState> = store => next => action => {
    const result = next(action);
    console.groupEnd();
    return result
}
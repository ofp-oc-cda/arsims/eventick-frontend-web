import axios from "axios";
import { useAppSelector, useAppDispatch } from "@/hooks/page";
import { setErrors, setLoading } from "@/redux/reducers/auth";

interface UseRequestProps {
  url: string;
  method: "get" | "post" | "patch" | "put" | "delete";
  body: any;
  onSuccess: any;
}

export default ({ url, method, body, onSuccess }: UseRequestProps) => {
  const errors = useAppSelector((state) => state.auth.errors);
  const loading = useAppSelector((state) => state.auth.loading);
  const dispatch = useAppDispatch();

  const doRequest = async () => {
    try {
      dispatch(setErrors(null));
      dispatch(setLoading(true));
      const response = await axios[method](url, body);
      if (onSuccess) {
        onSuccess(response.data);
      }
      return response.data;
    } catch (error: any) {
      dispatch(setLoading(false));
      if(typeof errors?.response !== "undefined") {
        dispatch(
        setErrors(
          <ul className="mt-0">
            {error.response.data.errors.map((error: any) => (
              <li key={error.message}>{error.message}</li>
            ))}
          </ul>
        )
      );
      }
    }
  };

  return { doRequest, errors, loading };
};

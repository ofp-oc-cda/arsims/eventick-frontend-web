export const posts = [
    {
      id: 1,
      title: "Refreshing Designs",
      description: "Experiment tracking and model registry for production teams",
      image: "https://cdn.devdojo.com/images/may2021/quench-satisfying.jpg",
      category: "Resources",
      backgroundColor: "rgb(168 85 247, 0.5)",
    },
    {
      id: 2,
      title: "Why Order in React Hooks Matters",
      description:
        "React Hooks are a new feature in React 16.8. They allow you to use state and other React features without writing a class. They are a powerful way to write stateful components, and they are a great way to write functional components.",
      image: "https://cdn.devdojo.com/images/may2021/orange.jpg",
      category: "Hooks",
      backgroundColor: "rgb(168 85 247, 0.5)",
    },
    {
      id: 3,
      title: "JavaScript Higher-Order Functions",
      description:
        "A function is a reusable piece of code designed to avoid repetitive code and improve code quality. As a functional programming language, JavaScript uses higher-order functions to implement this abstraction at an even higher level.",
      image: "https://cdn.devdojo.com/images/may2021/gbc.jpg",
      category: "Javascript",
      backgroundColor: "rgb(168 85 247, 0.5)",
    },
    {
      id: 4,
      title: "VMware reveals vSphere-as-a-service",
      description:
        "VMware today revealed details about Project Arctic, the vSphere-as-a-service offering it teased in late 2021, though it won't discuss pricing for another month.",
      image: "https://cdn.devdojo.com/images/may2021/quench-satisfying.jpg",
      category: "VMware",
      backgroundColor: "purple",
    },
    {
      id: 5,
      title: "Great (and Free!) Web Development Books You Can Get Online",
      description:
        "Right after “Where is the best place to learn?” perhaps the most commonly asked question I hear from folks getting into code is “What web development books should I get to learn?”",
      image: "https://cdn.devdojo.com/images/may2021/orange.jpg",
      category: "News",
      backgroundColor: "rgb(168 85 247, 0.5)",
    },
    {
      id: 6,
      title: "Build an ecommerce app with Next.js and Shopify",
      description:
        "With Shopify, not only can users set up an online store, but thanks to their Storefront API, developers can create a custom client application to connect to it.",
      image: "https://blog.logrocket.com/wp-content/uploads/2022/06/ecommerce-app-next-js-shopify.png",
      category: "E-Commerce",
      backgroundColor: "rgb(168 85 247, 0.5)",
    },
  ];
  
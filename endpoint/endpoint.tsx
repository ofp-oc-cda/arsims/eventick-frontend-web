import {EVENTICK_ENPOINT} from "./env";

//User endpoints
export const SIGNIN = `${EVENTICK_ENPOINT}/users/signin`;
export const SIGNUP = `${EVENTICK_ENPOINT}/users/signup`;
export const SIGNOUT = `${EVENTICK_ENPOINT}/users/signout`;
export const CURRENT_USER = `${EVENTICK_ENPOINT}/user/currentuser`;

//Ticket endpoints

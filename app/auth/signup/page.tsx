"use client";
import React from "react";
import { useRouter } from "next/navigation";
import { useAppSelector, useAppDispatch, useRequest } from "@/hooks/page";
import { setEmail, setPassword, setUsername } from "@/redux/reducers/auth";
import { SIGNUP } from "@/endpoint/endpoint";

const signup = () => {
  const email = useAppSelector((state) => state.auth.email);
  const password = useAppSelector((state) => state.auth.password);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { doRequest, errors, loading } = useRequest({
    url: SIGNUP,
    method: "post",
    body: {
      email,
      password,
    },
    onSuccess: () => router.push("/"),
  });

  const onSubmit = async (event: any) => {
    event.preventDefault();
    doRequest();
  };
  return (
    <section className="w-full bg-white dark:bg-gray-900">
      <div className="mx-auto max-w-7xl">
        <div className="flex flex-col lg:flex-row">
          <div className="relative w-full bg-cover lg:w-6/12 xl:w-7/12">
            <div className="relative my-20 flex h-full w-full flex-col items-center justify-center px-10 lg:my-0 lg:px-16">
              <div className="flex flex-col items-start space-y-8 tracking-tight lg:max-w-3xl">
                <div className="relative">
                  <p className="mb-2 font-medium uppercase text-gray-700">
                    Work smarter
                  </p>
                  <h2 className="text-5xl font-bold text-gray-900 dark:text-white xl:text-6xl">
                    Features to help you work smarter
                  </h2>
                </div>
                <p className="text-2xl text-gray-700">
                  We've created a simple formula to follow in order to gain more
                  out of your business and your application.
                </p>
                <a
                  href="/"
                  className="ease inline-block rounded-lg bg-blue-600 px-8 py-5 text-center text-xl font-medium text-white transition duration-200 hover:bg-blue-700"
                >
                  Get Started Today
                </a>
              </div>
            </div>
          </div>

          <form
            onSubmit={onSubmit}
            className="h-screen w-full bg-gray-100 dark:bg-gray-800 lg:w-6/12 xl:w-5/12"
          >
            <div className="flex h-full w-full flex-col items-center justify-center p-10 lg:p-16 xl:p-24">
              <h4 className="w-full justify-center text-center py-5 text-5xl font-bold dark:text-white">
                Signup
              </h4>
              <p className="text-lg text-gray-500">
                if you have an account you can{" "}
                <a href="/signin" className="text-blue-600 underline">
                  sign in
                </a>
              </p>
              <div className="relative mt-10 w-full space-y-8">
                {/* <input
                  type="text"
                  name="text"
                  value={username}
                  onChange={(e: any) => dispatch(setUsername(e.target.value))}
                  className="mb-4 block w-full rounded-lg border-2 border-gray-200 border-transparent px-4 py-3 focus:outline-none focus:ring focus:ring-blue-500 dark:bg-gray-700"
                  placeholder="Username"
                /> */}
                <input
                  type="email"
                  name="email"
                  value={email}
                  onChange={(e: any) => dispatch(setEmail(e.target.value))}
                  className="mb-4 block w-full rounded-lg border-2 border-gray-200 border-transparent px-4 py-3 focus:outline-none focus:ring focus:ring-blue-500 dark:bg-gray-700"
                  placeholder="Email address"
                />
                <input
                  type="password"
                  name="password"
                  value={password}
                  onChange={(e: any) => dispatch(setPassword(e.target.value))}
                  className="mb-4 block w-full rounded-lg border-2 border-gray-200 border-transparent px-4 py-3 focus:outline-none focus:ring focus:ring-blue-500 dark:bg-gray-700"
                  placeholder="Password"
                />
                {errors && (
                  <div className="alert alert-error mb-5 shadow-lg">
                  <div>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6 flex-shrink-0 stroke-current"
                      fill="none"
                      viewBox="0 0 24 24"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                    {errors}
                  </div>
                </div>
                )}
                <div className="block">
                  <button className="w-full rounded-lg bg-blue-600 px-3 py-4 font-medium text-white">
                    {loading ? "Loading ... " : "Create account"}
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
};

export default signup;

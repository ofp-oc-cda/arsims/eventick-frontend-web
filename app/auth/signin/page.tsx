"use client";

import React from "react";
import { useAppSelector, useAppDispatch, useRequest } from "@/hooks/page";
import { setEmail, setPassword } from "@/redux/reducers/auth";
import { useRouter } from "next/navigation";
import {SIGNIN} from "@/endpoint/endpoint";

const signin = () => {
  const email = useAppSelector((state) => state.auth.email);
  const password = useAppSelector((state) => state.auth.password);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { doRequest, errors, loading } = useRequest({
    url: SIGNIN,
    method: "post",
    body: {
      email,
      password,
    },
    onSuccess: () => router.push("/"),
  });

  const onSubmit = async (event: any) => {
    event.preventDefault();
    await doRequest();
  };

  return (
    <section className="flex h-screen w-full items-center justify-center bg-gray-100 px-8 py-16 dark:bg-gray-900 xl:px-8">
      <div className="mx-auto max-w-5xl">
        <div className="flex flex-col items-center md:flex-row">
          <div className="w-full space-y-5 md:w-3/5 md:pr-16">
            <p className="font-medium uppercase text-blue-500">
              Start Sell your tickets
            </p>
            <h2 className="text-2xl font-extrabold leading-none text-black dark:text-white sm:text-3xl md:text-5xl">
              Changing The Way People buy tickets
            </h2>
            <p className="text-xl text-gray-600 md:pr-16">
              Learn how to engage with your visitors and teach them about your
              mission. We're revolutionizing the way customers and businesses
              interact.
            </p>
          </div>
          <form onSubmit={onSubmit} className="mt-16 w-full md:mt-0 md:w-2/5">
            <div className="relative z-10 h-auto overflow-hidden rounded-lg border-b-2 border-gray-300 bg-white p-8 px-7 py-10 shadow-2xl dark:bg-gray-800">
              <h3 className="mb-6 text-center text-2xl font-medium dark:text-white">
                Sign in to your Account
              </h3>
              <input
                type="email"
                name="email"
                value={email}
                required={false}
                onChange={(e: any) => dispatch(setEmail(e.target.value))}
                className="mb-4 block w-full rounded-lg border-2 border-gray-200 border-transparent px-4 py-3 focus:outline-none focus:ring focus:ring-blue-500 dark:bg-gray-700"
                placeholder="Email address"
              />
              <input
                type="password"
                name="password"
                value={password}
                onChange={(e: any) => dispatch(setPassword(e.target.value))}
                className="mb-4 block w-full rounded-lg border-2 border-gray-200 border-transparent px-4 py-3 focus:outline-none focus:ring focus:ring-blue-500 dark:bg-gray-700"
                placeholder="Password"
              />
              {errors && (
                <div className="alert alert-error mb-5 shadow-lg">
                <div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 flex-shrink-0 stroke-current"
                    fill="none"
                    viewBox="0 0 24 24"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                  {errors}
                </div>
              </div>
              )}
              <div className="block">
                <button className="w-full rounded-lg bg-blue-600 px-3 py-4 font-medium text-white">
                  {loading ? "Loading ... " : "Log me in"}
                </button>
              </div>
              <p className="mt-4 w-full text-center text-sm text-gray-500">
                Don't have an account?{" "}
                <a href="/auth/signup" className="text-blue-500 underline">
                  Sign up here
                </a>
              </p>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
};

export default signin;

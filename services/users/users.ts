import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {EVENTICK_ENPOINT} from "@/endpoint/env";

export const authApi = createApi({
    reducerPath: 'users',
    baseQuery: fetchBaseQuery({
        baseUrl: EVENTICK_ENPOINT
    }),
    endpoints: (builder) => ({
        authCurrentUser: builder.query({
            query: () => `users/currentuser`
        }),
        authSignin: builder.mutation({
            query: () => `users/signin`
        }),
        authSignup: builder.mutation({
            query: () => `users/signup`
        }),
        authSignout: builder.mutation({
            query: () => `users/signout`
        })
    })
});

export const {useAuthCurrentUserQuery, useAuthSignoutMutation, useAuthSignupMutation, useAuthSigninMutation} = authApi

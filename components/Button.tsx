const Button = ({ children, className, onClick }: any) => {
    return (
      <button
        className={`p-5 hover:scale-105 transition-transform ${className}`}
        onClick={onClick}
      >{children}</button>
    )
  }
  
  export default Button
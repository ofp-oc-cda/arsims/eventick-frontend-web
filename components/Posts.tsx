"use client"

import React, {useState} from "react";
import Post from "./Post";
import { posts } from "@/data/PostData";
import CategoryTags from "./CategoryTags";

const Posts = () => {
  const [search, setSearch] = useState<string>("")

  return (
    <section className="relative w-full bg-white dark:bg-gray-800">
      <div className="relative w-full px-5 py-10 mx-auto sm:py-12 md:py-16 md:px-10 max-w-7xl">
        <h1 className="text-4xl font-extrabold leading-none text-gray-900 lg:text-5xl xl:text-6xl sm:mb-3 dark:text-white">
          <a href="#_">The Event that you wanted</a>
        </h1>
        <p className="text-lg font-medium text-gray-500 sm:text-2xl">
          Choose your favorite event and get the best deals.
        </p>
        <div className="inline-flex w-full mt-5 items-center justify-between">
          <div className="hidden lg:inline-flex space-x-2 text-gray-800 whitespace-nowrap">
            {posts.map((post) => (
              <CategoryTags key={post.id} category={post.category} />
            ))}
          </div>
          <input
            type="text"
            name="search"
            onChange={(e: any) => setSearch(e.target.value)}
            className="block w-80 px-4 py-3 border dark:border-transparent dark:bg-gray-700 border-gray-400 rounded-lg focus:ring focus:ring-blue-500 focus:outline-none"
            placeholder="Search tickets .. "
          ></input>
        </div>

        <div className="grid h-full grid-cols-12 gap-10 pb-10 mt-8 sm:mt-16">
          {posts.filter((event: any) => {
            return (event.title).includes(search);
          }).map((post) => (
            <Post
              key={post.id}
              title={post.title}
              description={post.description}
              image={post.image}
              category={post.category}
              backgroundColor={post.backgroundColor}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

export default Posts;

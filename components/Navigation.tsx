"use client";
import React from "react";
import { useTheme } from "next-themes";
import Button from "./Button";

interface NavigationProps {
  t?: any;
  changeLanguage?: any;
  locale?: any
}

const Navigation = ({t, changeLanguage, locale}: NavigationProps) => {
  const { theme, setTheme, systemTheme } = useTheme();
  const currentUser = false;
  const name = "Arsim";

  const renderThemeChanger = () => {
    const currentTheme = theme === "system" ? systemTheme : theme;

    if (currentTheme === "dark") {
      return (
        <Button
          classname="flex-col justify-center items-center"
          onClick={() => setTheme("light")}
        >
          <svg
            className="swap-on h-8 w-8 fill-current text-white"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
          >
            <path d="M5.64,17l-.71.71a1,1,0,0,0,0,1.41,1,1,0,0,0,1.41,0l.71-.71A1,1,0,0,0,5.64,17ZM5,12a1,1,0,0,0-1-1H3a1,1,0,0,0,0,2H4A1,1,0,0,0,5,12Zm7-7a1,1,0,0,0,1-1V3a1,1,0,0,0-2,0V4A1,1,0,0,0,12,5ZM5.64,7.05a1,1,0,0,0,.7.29,1,1,0,0,0,.71-.29,1,1,0,0,0,0-1.41l-.71-.71A1,1,0,0,0,4.93,6.34Zm12,.29a1,1,0,0,0,.7-.29l.71-.71a1,1,0,1,0-1.41-1.41L17,5.64a1,1,0,0,0,0,1.41A1,1,0,0,0,17.66,7.34ZM21,11H20a1,1,0,0,0,0,2h1a1,1,0,0,0,0-2Zm-9,8a1,1,0,0,0-1,1v1a1,1,0,0,0,2,0V20A1,1,0,0,0,12,19ZM18.36,17A1,1,0,0,0,17,18.36l.71.71a1,1,0,0,0,1.41,0,1,1,0,0,0,0-1.41ZM12,6.5A5.5,5.5,0,1,0,17.5,12,5.51,5.51,0,0,0,12,6.5Zm0,9A3.5,3.5,0,1,1,15.5,12,3.5,3.5,0,0,1,12,15.5Z" />
          </svg>
        </Button>
      );
    } else {
      return (
        <Button
          classname="flex-col justify-center items-center"
          onClick={() => setTheme("dark")}
        >
          <svg
            className="swap-off h-8 w-8 fill-current text-white"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
          >
            <path d="M21.64,13a1,1,0,0,0-1.05-.14,8.05,8.05,0,0,1-3.37.73A8.15,8.15,0,0,1,9.08,5.49a8.59,8.59,0,0,1,.25-2A1,1,0,0,0,8,2.36,10.14,10.14,0,1,0,22,14.05,1,1,0,0,0,21.64,13Zm-9.5,6.69A8.14,8.14,0,0,1,7.08,5.22v.27A10.15,10.15,0,0,0,17.22,15.63a9.79,9.79,0,0,0,2.1-.22A8.11,8.11,0,0,1,12.14,19.73Z" />
          </svg>
        </Button>
      );
    }
  };

  return (
    <section className="sticky top-0 z-50 w-full bg-indigo-600 px-3 antialiased lg:px-6">
      <div className="mx-auto max-w-7xl">
        <nav
          x-data="{ showMenu: true }"
          className="flex h-24 w-full select-none items-center"
        >
          <div className="relative mx-auto flex h-24 w-full flex-wrap items-center justify-between font-medium md:justify-center">
            <a
              href="/"
              className="w-1/4 cursor-pointer py-4 pl-6 pr-4 md:py-0 md:pl-4"
            >
              <span className="select-none p-1 text-xl font-black leading-none text-white">
                <span>EventPro</span>
                <span className="text-indigo-300">.</span>
              </span>
            </a>
            <div className="fixed left-0 top-0 z-40 hidden h-full w-full items-center bg-gray-900 bg-opacity-50 p-3 text-xl md:relative md:flex md:w-3/4 md:bg-transparent md:p-0 md:text-sm lg:text-base">
              <div className="h-full w-full select-none flex-col overflow-hidden rounded-lg bg-white md:relative md:flex md:flex-row md:overflow-auto md:rounded-none md:bg-transparent">
                <div className="mt-12 flex h-full w-full flex-col items-center justify-center text-center text-indigo-700 md:mt-0 md:w-2/3 md:flex-row md:items-center md:text-indigo-200">
                  <a
                    href="/ticketss"
                    className="mx-2 inline-block px-4 py-2 text-left font-medium text-indigo-700 md:px-0 md:text-center md:text-white lg:mx-3"
                  >
                    Create Tickets
                  </a>
                  <a
                    href="/features"
                    className="mx-2 inline-block px-4 py-2 text-left font-medium hover:text-indigo-800 md:px-0 md:text-center md:hover:text-white lg:mx-3"
                  >
                    Find Events
                  </a>
                  {currentUser && (
                    <a
                      href="/dashboard"
                      className="mx-2 inline-block px-4 py-2 text-left font-medium hover:text-indigo-800 md:px-0 md:text-center md:hover:text-white lg:mx-3"
                    >
                      dashboard
                    </a>
                  )}

                  <a
                    href="/about"
                    className="mx-2 inline-block px-4 py-2 text-left font-medium hover:text-indigo-800 md:px-0 md:text-center md:hover:text-white lg:mx-3"
                  >
                    About
                  </a>
                  <a
                    href="/contact"
                    className="mx-2 inline-block px-4 py-2 text-left font-medium hover:text-indigo-800 md:px-0 md:text-center md:hover:text-white lg:mx-3"
                  >
                    Contact
                  </a>
                </div>
                {currentUser ? (
                  <div className="flex h-full w-full flex-col items-center justify-end pt-4 md:w-1/3 md:flex-row md:py-0">
                    <p className="mr-0 w-full pl-6 text-indigo-200 hover:text-white md:mr-3 md:w-auto md:pl-0 lg:mr-5">
                      Welcome back, {name}
                    </p>
                    <a
                      href="/auth/signout"
                      className="whitespace-no-wrap focus:shadow-outline-indigo mr-1 inline-flex items-center justify-center rounded-full border border-transparent bg-white px-4 py-2 text-base font-medium leading-6 text-indigo-600 transition duration-150 ease-in-out hover:bg-white focus:border-indigo-700 focus:outline-none active:bg-indigo-700"
                    >
                      Sign Out
                    </a>
                  </div>
                ) : (
                  <div className="flex h-full w-full flex-col items-center justify-end pt-4 md:w-1/3 md:flex-row md:py-0">
                    <a
                      href="/auth/signin"
                      className="mr-0 w-full pl-6 text-indigo-200 hover:text-white md:mr-3 md:w-auto md:pl-0 lg:mr-5"
                    >
                      Sign In
                    </a>
                    <a
                      href="/auth/signup"
                      className="whitespace-no-wrap focus:shadow-outline-indigo mr-1 inline-flex items-center justify-center rounded-full border border-transparent bg-white px-4 py-2 text-base font-medium leading-6 text-indigo-600 transition duration-150 ease-in-out hover:bg-white focus:border-indigo-700 focus:outline-none active:bg-indigo-700"
                    >
                      Sign Up
                    </a>
                  </div>
                )}
              </div>
              {renderThemeChanger()}
              <select
                defaultValue={"en"}
                className="text-shadow-sm bg-transparent text-md tracking-wide text-white border-none"
              >
                <option className="text-black p-10" value="en">
                  EN
                </option>
                <option className="text-black p-10" value="fr">
                  FR
                </option>
              </select>
            </div>
            <div className="absolute right-0 z-50 mr-4 flex h-10 w-10 cursor-pointer flex-col items-end rounded-full p-2 hover:bg-gray-900 hover:bg-opacity-10 md:hidden">
              <svg
                className="h-6 w-6"
                x-show="!showMenu"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                x-cloak=""
              >
                <path d="M4 6h16M4 12h16M4 18h16"></path>
              </svg>
              <svg
                className="h-6 w-6"
                x-show="showMenu"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                x-cloak=""
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                ></path>
              </svg>
            </div>
          </div>
        </nav>
      </div>
    </section>
  );
};

export default Navigation;
